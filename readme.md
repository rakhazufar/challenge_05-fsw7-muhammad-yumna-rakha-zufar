
# Binar Cars Admin

Challenge dari chapter ke 5




## Untuk memulai aplikasi
Jalankan perintah ini untuk menjalankan database
```npm
yarn start
```



## endpoints
To:

Link halaman Utama:
[a link](http://localhost:8000/)

Link Halaman Add Car:
[a link](http://localhost:8000/addCar)

Add Cars: /cars

get Cars: /carList

Delete Car: /deleteCar/:id

Edit Car: /updateCar/:id


## Database Diagram

![Db Diagram](assets/image/Untitled.png)

