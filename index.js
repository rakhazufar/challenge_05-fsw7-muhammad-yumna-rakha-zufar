const { Cars, Sizes } = require("./models");

// Sizes.create({ name: "small" });
// Sizes.create({ name: "medium" });
// Sizes.create({ name: "large" });

const express = require("express");
const app = express();
const ejs = require("ejs");
const fs = require("fs");
const multer = require("multer");

const { PORT = 8000 } = process.env;

app.engine("html", ejs.renderFile);
app.set("view engine", "html");

express.json();

const form = multer({ dest: "uploads" });
app.use(express.static(__dirname + "/assets"));
app.use("/uploads", express.static("uploads"));

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/addCar/:id", async (req, res) => {
  let cars = await Sizes.findAll();
  let dataCar = {
    name: "",
    price: "",
    size_id: "",
    image: "",
    action: "/cars",
  };
  if (parseInt(req.params.id) !== 0) {
    dataCar = await Cars.findOne({
      where: {
        id: req.params.id,
      },
    });
    dataCar.action = `/updateCar/${dataCar.id}`;
  }

  res.render("addCar", { data: cars, carValue: dataCar });
});

app.post("/cars", form.single("image"), (req, res) => {
  let inputCar = req.body;

  if (req.file) {
    let image = req.file.originalname;
    let temp = req.file.path;
    fs.copyFileSync(temp, `uploads/${image}`);
    fs.unlinkSync(temp);
    inputCar.image = `uploads/${image}`;
  }

  Cars.create({
    name: inputCar.nama,
    price: inputCar.sewaPerHari,
    size_id: inputCar.size,
    image: inputCar.image,
  });

  res.redirect("/");
});

app.get("/carList", async (req, res) => {
  const data = await Cars.findAll();
  res.status(200).json(data);
});

app.delete("/deleteCar/:id", async (req, res) => {
  const id = req.params.id;
  const car = await Cars.findOne({
    where: {
      id: id,
    },
  });

  Cars.destroy({
    where: {
      id: id,
    },
  })
    .then(() => {
      console.log("data berhasil dihapus");
    })
    .catch((error) => {
      console.log(error);
    });

  fs.unlink(car.image, () => console.log("data berhasil didelete"));
});

app.post("/updateCar/:id", form.single("image"), async (req, res) => {
  let inputCar = req.body;
  console.log(inputCar);

  let prevCar = await Cars.findOne({
    where: {
      id: req.params.id,
    },
  });

  fs.unlink(prevCar.image, () => console.log("data berhasil didelete"));

  if (req.file) {
    let image = req.file.originalname;
    let temp = req.file.path;
    fs.copyFileSync(temp, `uploads/${image}`);
    fs.unlinkSync(temp);
    inputCar.image = `uploads/${image}`;
  }

  Cars.update(
    {
      name: inputCar.nama,
      price: inputCar.sewaPerHari,
      size_id: inputCar.size,
      image: inputCar.image,
    },
    {
      where: {
        id: req.params.id,
      },
    }
  ).then(() => {
    res.redirect("/");
  });
});

// app.put("/updateCar/:id", (req, res) => {
//   let inputCar = req.body.data;
//   console.log(inputCar);
//   // Cars.update(
//   //   {
//   //     name: inputCar.nama,
//   //     price: inputCar.sewaPerHari,
//   //     size_id: inputCar.size,
//   //     image: inputCar.image,
//   //   },
//   //   {
//   //     where: {
//   //       id: req.params.id,
//   //     },
//   //   }
//   // ).then(() => {
//   //   res.redirect("/");
//   // });
// });

app.listen(PORT, () => {
  console.log(`server running in port ${PORT}`);
});
