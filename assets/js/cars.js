const api = axios.create({
  baseURL: "http://localhost:8000",
});

let listCars = document.querySelector(".listCars");
let name = document.getElementById("name");
let price = document.getElementById("price");
let image = document.getElementById("image");
let update_at = document.getElementById("update_at");
let container_car = document.querySelector(".list-car");

function showCars(Cars) {
  let listCar = "";
  Cars.map((car) => {
    listCar += `<div class="card" style="width: 18rem">
              <img src="${car.image}" class="card-img-top car-img" alt="..." id="image" />
              <div class="card-body">
                <p class="card-title" id="name">${car.name}</p>
                <h5 class="card-text" id="price">Rp. ${car.price} / Hari</h5>
                <p class="card-text update_at" id="update_at"> <img src="image/fi_clock.svg" class="user" alt="" /> Update at ${car.updatedAt.slice(0,10)}</p>
                
                <a onclick="deleteCar(${car.id})" class="btn btn-delete btn-outline-danger"
                  ><img src="image/Vector.svg" alt="" /> Delete</a>

                <a onclick='addCar(${car.id})' class="btn btn-edit btn-success">
                  <img src="image/fi_edit.svg" alt="" /> Edit</a
                >
              </div>
              </div>`;
  });

  let listCars = document.querySelector(".listCars");
  listCars.innerHTML = listCar;
}

function getCars() {
  api
    .get("/carList")
    .then((result) => {
      showCars(result.data);
    })
    .catch(() => {
      throw new Error("something error");
    });
}

function deleteCar(id) {
  api
    .delete(`/deleteCar/${id}`)
    .then((success) => {
      console.log(success);
    })
    .catch((error) => {
      console.log(error);
    });

  window.location.reload();
}

function addCar(id) {
  api
    .get(`/addCar/${id}`)
    .then((success) => {
      container_car.innerHTML = success.data;
    })
    .catch((error) => {
      console.log(error);
    });
}

getCars();
